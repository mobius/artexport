<?php
defined('_JEXEC') or die('Restricted access');
$database = $db =& JFactory::getDBO();

function formMenu() {
    echo "<table width='100%' border='0'>\n"
            . "<tr>\n"
            . "    <td width='100%'>&nbsp;</td>\n"
            . "    <td align='center'>\n"
            . "        <a class='toolbar' onclick='saveSettings()' href='#'>\n"
            . "            <img src='/administrator/images/save_f2.png' alt='Сохранить' name='save' title='Сохранить'\n"
            . "                 align='middle' border='0'>\n"
            . "            <br>Сохранить\n"
            . "        </a>\n"
            . "    </td>\n"
            . "    <td width='10'>&nbsp;</td>\n"
            . "    <td align='center'>\n"
            . "        <a class='toolbar' onclick='applySettings()' href='#'>\n"
            . "            <img src='/administrator/images/apply_f2.png' alt='Применить' name='apply' title='Применить'\n"
            . "                 align='middle' border='0'>\n"
            . "            <br>Применить\n"
            . "        </a>\n"
            . "    </td>\n"
            . "    <td width='10'>&nbsp;</td>\n"
            . "    <td align='center'>\n"
            . "        <a class='toolbar' href='?option=com_artimport&task=channels_list'>\n"
            . "            <img src='/administrator/images/cancel_f2.png' alt='Отмена' name='cancel' title='Отмена'\n"
            . "                 align='middle' border='0'>\n"
            . "            <br>Отмена\n"
            . "        </a>\n"
            . "    </td>\n"
            . "</tr>\n"
            . "</table>\n";
}

$database->setQuery("SELECT * FROM #__artimport_config");
$settings = $database->loadObjectList();
$setting = $settings[0];

formMenu();
?>

<script type="text/javascript">

    function saveSettings() {
        var task = document.getElementById("task");
        var theForm = document.getElementById("theForm");
        task.value = "save_settings";
        theForm.submit();
        return true;
    }

    function applySettings() {
        var task = document.getElementById("task");
        var theForm = document.getElementById("theForm");
        task.value = "apply_settings";
        theForm.submit();
        return true;
    }
</script>


<form method="POST" id="theForm">
    <input type="hidden" id="task" name="task" value="save_channel">
    <table class="adminheading" width="100%" border="0">
        <tr>
            <th class="edit" colspan="5">Настройки импорта</th>
        </tr>
        <tr>
            <td nowrap="nowrap"><label for="userName">Пользователь:</label></td>
            <td width="20" rowspan="5">&nbsp;</td>
            <td>
                <input id="userName" name="userName" type="text" value="<?php echo $setting->username; ?>">
            </td>
            <td width="20" rowspan="5">&nbsp;</td>
            <td width="100%" rowspan="2">
                <small>
                    Пользователь должен быть <a
                        href="http://chugaga.net/component/user/register" target="_blank">зарегистрирован</a>
                    на сайте <a href="http://chugaga.net" target="_blank">chugaga.net</a>
                </small>
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap"><label for="password">Пароль:</label></td>
            <td>
                <input id="password" name="password" type="password">
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap"><label for="language">Язык импортируемых/экспортируемых статей:</label></td>
            <td>
                <select id="language" name="language">
                    <option value=""<?php if ($setting->language == "" || !isset($setting->language)) {
                        echo " selected";
                    }?>>Все языки
                    </option>
                    <option value="ENG"<?php if ($setting->language == "ENG") {
                        echo " selected";
                    }?>>English
                    </option>
                    <option value="RUS"<?php if ($setting->language == "RUS") {
                        echo " selected";
                    }?>>Русский
                    </option>
                </select>
            </td>
            <td width="100%">
                <small>
                    Язык публикаций, с которыми будет работать компонент ArtImport.
                </small>
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap"><label for="updateinterval">Интервал обновлений:</label></td>
            <td>
                <select id="updateinterval" name="updateinterval">
                    <option value="1"></option>
                <?php
                    for ($i = 0; $i <= 24; $i++) {
                    echo "<option value='" . $i . "'" . (($setting->update_interval == $i) ? " selected" : "") . ">" . $i . "</option>";
                }
                ?>
                </select>
            </td>
            <td width="100%">
                <small>
                    Минимальное количество часов, через которое будут загружаться ноости. 0 - ограничений нет (не
                    рекомендуется, так как это вызовет сильную нагрузку на Вашу систему).
                </small>
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap"><label for="articleowner">Владелец статей:</label></td>
            <td>
                <select id="articleowner" name="articleowner">
                <?php
                $database->setQuery("SELECT * FROM #__users where block=0");
                $users = $database->loadObjectList();
                if (sizeof($users) > 0) {
                    foreach ($users as $user) {
                        echo "<option value='" . $user->id . "'" . (($setting->article_user_owner_id == $user->id) ? " selected" : "") . ">" . $user->username . " (" . $user->name . ")</option>";
                    }
                }
                ?>
                </select>
            </td>
            <td width="100%">
                <small>
                    Необходимо указать, кому из пользователей Вашего портала будут принаждежать импортированные статьи.
                </small>
            </td>
        </tr>
    </table>
</form>