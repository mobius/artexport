<?php
defined('_JEXEC') or die('Restricted access');
$database = $db =& JFactory::getDBO();
?>
<script type="text/javascript">
    function deleteChannel() {
        if (confirm("Вы действительно ходите удалить выбранные каналы?")) {
            var theForm = document.getElementById("theForm");
            theForm.submit()
        }
    }
</script>
<table width="100%" border="0">
    <tr>
        <td width="100%">&nbsp;</td>
        <td align="center">
            <a class="toolbar" href="?option=com_artimport&task=export_channel">
                <img src="/administrator/images/new_f2.png" width="32" height="32" alt="Новый" name="new" title="Новый"
                     align="middle" border="0">
                <br>Новый
            </a>
        </td>
        <td width="10">&nbsp;</td>
        <td align="center">
            <a class="toolbar" onclick="deleteChannel();" href="#">
                <img src="/administrator/images/delete_f2.png" width="32" height="32" alt="Удалить" name="delete"
                     title="Удалить"
                     align="middle" border="0">
                <br>Удалить
            </a>
        </td>
    </tr>
</table>

<table class="adminheading" width="100%" border="0">
    <tr>
        <th class="edit" colspan="5">Список экспортируемых каналов</th>
    </tr>
</table>
<form id="theForm" method="POST">
    <input type="hidden" name="task" value="delete_channel">
    <table width="100%" border="0" class="adminlist">
        <thead>
        <tr>
            <th width="20">&nbsp;</th>
            <th width="30">#</th>
            <th width="*">Канал</th>
            <th width="*">Раздел</th>
            <th width="*">Категория</th>
            <th width="80">Публикация</th>
        </tr>
        </thead>
    <?php

    $sql = "SELECT channels.*, sections.title as section_title, categories.title as category_title FROM #__artimport_export_channels as channels\n"
            . "LEFT OUTER JOIN #__categories as categories ON categories.id = channels.category_id\n"
            . "LEFT OUTER JOIN #__sections as sections ON sections.id = categories.section";

    $database->setQuery($sql);
    $channels = $database->loadObjectList();

    $rowId = 0;
    foreach ($channels as $channel) {
        echo "<tr  class='row" . $rowId . "'>"
                . "<td width='20'><input type='checkbox' name='channel_ids' value='" . $channel->id . "'></td>"
                . "<td width='30'>" . $channel->id . "</td>"
                . "<td width='33%'><a alt='Редактировать канал' title='Редактировать канал' href='?option=com_artimport&task=export_channel&channel_id=" . $channel->id . "'>" . $channel->channel_name . "</a></td>"
                . "<td width='33%'>" . $channel->section_title . "</td>"
                . "<td width='33%'>" . $channel->category_title . "</td>"
                . "<td width='80'>";
        if ($channel->active == 1) {
            echo "<a href = '?option=com_artimport&task=inactivate_exp_channel&channel_id=" . $channel->id . "' ><img src='/administrator/images/publish_g.png' width='12' height='12' border='0'></a >";
        } else {
            echo "<a href = '?option=com_artimport&task=activate_exp_channel&channel_id=" . $channel->id . "' ><img src='/administrator/images/publish_x.png' width='12' height='12' border='0'></a >";
        }

        echo "</td>"
                . "</tr>";

        if ($rowId == 0) {
            $rowId = 1;
        } else {
            $rowId = 0;
        }
    }
    ?>
    </table>
</form>