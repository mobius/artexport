<?php
defined('_JEXEC') or die('Restricted access');

function deleteChannel($channelId) {
    $database = & JFactory::getDBO();

    if (is_array($channelId)) {
        if (sizeof($channelId) > 0) {
            $sql = "DELETE FROM #__artimport_import_channels WHERE id IN (";
            $commaFlag = false;
            foreach ($channelId as $id) {
                if ($commaFlag) {
                    $sql = $sql . ", ";
                }
                $sql = $sql . $id;
            }
            $sql = $sql . ")";
            $database->setQuery($sql);
            $database->query();
        }
    } else {
        $database->setQuery("DELETE FROM #__artimport_import_channels WHERE id = " . (int) $channelId);
        $database->query();
    }
}

function deleteExpChannel($channelId) {
    $database = & JFactory::getDBO();

    if (is_array($channelId)) {
        if (sizeof($channelId) > 0) {
            $sql = "DELETE FROM #__artimport_export_channels WHERE id IN (";
            $commaFlag = false;
            foreach ($channelId as $id) {
                if ($commaFlag) {
                    $sql = $sql . ", ";
                }
                $sql = $sql . $id;
            }
            $sql = $sql . ")";
            $database->setQuery($sql);
            $database->query();
        }
    } else {
        $database->setQuery("DELETE FROM #__artimport_export_channels WHERE id = " . (int) $channelId);
        $database->query();
    }
}

function saveChannel($channelId, $category, $channelName, $autoPublish, $mainPage, $active) {
    $database = & JFactory::getDBO();

    deleteChannel($channelId);

    $database->setQuery("INSERT INTO #__artimport_import_channels (id, category_id, channel_name, autopublish, mainpage, active) values ("
            . (int) $channelId . ", " . (int) $category . ", " . $database->Quote($channelName) . ", " . $autoPublish . ", "
            . $mainPage . ", " . (int) $active . ")");
    $database->query();
}

function saveExpChannel($channelId, $category, $channelName, $active) {
    $database = & JFactory::getDBO();

    deleteExpChannel($channelId);

    $database->setQuery("INSERT INTO #__artimport_export_channels (id, category_id, channel_name, active) values ("
            . (int) $channelId . ", " . (int) $category . ", " . $database->Quote($channelName) . ", "
            . (int) $active . ")");
    $database->query();
}

function activateChannel($channelId) {
    $database = & JFactory::getDBO();

    $database->setQuery("UPDATE #__artimport_import_channels set active = 1 WHERE id = " . (int) $channelId);
    $database->query();
}

function activateExpChannel($channelId) {
    $database = & JFactory::getDBO();

    $database->setQuery("UPDATE #__artimport_export_channels set active = 1 WHERE id = " . (int) $channelId);
    $database->query();
}

function inactivateChannel($channelId) {
    $database = & JFactory::getDBO();

    $database->setQuery("UPDATE #__artimport_import_channels set active = 0 WHERE id = " . (int) $channelId);
    $database->query();
}

function inactivateExpChannel($channelId) {
    $database = & JFactory::getDBO();

    $database->setQuery("UPDATE #__artimport_export_channels set active = 0 WHERE id = " . (int) $channelId);
    $database->query();
}

function saveSettings($username, $password, $articleOwner, $updateInterval, $language) {
    $database = & JFactory::getDBO();

    $sql = "UPDATE #__artimport_config set username = " . $database->Quote($username) . ", article_user_owner_id = "
            . $articleOwner . ", update_interval = " . $updateInterval . ", last_updated = NOW()";

    if ($password != "") {
        $sql = $sql . ", password = " . $database->Quote($password);
    }

    if ($language != null && $language != "") {
        $sql = $sql . ", language = " . $database->Quote($language);
    } else {
        $sql = $sql . ", language = null";
    }

    $database->setQuery($sql);
    $database->query();
}

//Операция, которая будет производиться
$operation = JRequest::getVar("task", null);
//Идентификатор канала
$channelId = intval(JRequest::getVar("channel_id", null));
//Идентификаторы каналов
$channelIds = JRequest::getVar("channel_ids", null);
//Имя канала
$channelName = JRequest::getVar("channel_name", null);
//Идентификатор категории
$category = JRequest::getVar("category", null, "post", "INT");
//Флаг активности
$activeChannel = JRequest::getVar("active", 1, "post", "INT");
$userName = JRequest::getVar("userName", null);
$password = JRequest::getVar("password", null);
$autoPublish = JRequest::getVar("autopublish", 1, "post", "INT");
$mainPage = JRequest::getVar("mainpage", 1, "post", "INT");
$articleOwner = JRequest::getVar("articleowner", 62);
$updateInterval = JRequest::getVar("updateinterval", 1);
$contentLanguage = JRequest::getVar("language", null, "post");

$bodyPage = "admin.artimport.list_import_channels.php";
$messages = "";

switch ($operation) {
    case "export_channels_list":
        $bodyPage = "admin.artimport.list_export_channels.php";
        break;

    case "export_channel":
        $bodyPage = "admin.artimport.export_channel.php";
        break;

    case "import_channel":
        $bodyPage = "admin.artimport.import_channel.php";
        break;

    case "save_channel":
        saveChannel($channelId, $category, $channelName, $autoPublish, $mainPage, $activeChannel);
        $messages = "Канал успешно сохранён";
        break;

    case "save_exp_channel":
        $bodyPage = "admin.artimport.list_export_channels.php";
        saveExpChannel($channelId, $category, $channelName, $activeChannel);
        $messages = "Канал успешно сохранён";
        break;

    case "apply_channel":
        $bodyPage = "admin.artimport.import_channel.php";
        saveChannel($channelId, $category, $channelName, $autoPublish, $mainPage, $activeChannel);
        $messages = "Канал успешно сохранён";
        break;

    case "apply_exp_channel":
        $bodyPage = "admin.artimport.export_channel.php";
        saveExpChannel($channelId, $category, $channelName, $activeChannel);
        $messages = "Канал успешно сохранён";
        break;

    case "activate_channel":
        activateChannel($channelId);
        $messages = "Канал активирован";
        break;

    case "activate_exp_channel":
        $bodyPage = "admin.artimport.list_export_channels.php";
        activateChannel($channelId);
        $messages = "Канал активирован";
        break;

    case "inactivate_channel":
        inactivateChannel($channelId);
        $messages = "Канал деактивирован";
        break;

    case "inactivate_exp_channel":
        $bodyPage = "admin.artimport.list_export_channels.php";
        inactivateExpChannel($channelId);
        $messages = "Канал деактивирован";
        break;

    case "delete_channel":
        deleteChannel($channelIds);
        $messages = "Выбранные каналы удалены";
        break;

    case "delete_exp_channel":
        $bodyPage = "admin.artimport.list_export_channels.php";
        deleteExpChannel($channelIds);
        $messages = "Выбранные каналы удалены";
        break;

    case "settings":
        $bodyPage = "admin.artimport.settings.php";
        break;

    case "save_settings":
        saveSettings($userName, $password, $articleOwner, $updateInterval, $contentLanguage);
        $messages = "Настройки сохранены";
        break;

    case "apply_settings":
        $bodyPage = "admin.artimport.settings.php";
        saveSettings($userName, $password, $articleOwner, $updateInterval, $contentLanguage);
        $messages = "Настройки сохранены";
        break;
}

if ($messages != null && $messages != "") {
    echo "<table width='100%' cellspacing='0' cellpadding='0' border='0'>"
            . "<tr><td align='center'><b>" . $messages . "</b></td></tr></table>";

    $messages = "";
}

include "components/com_artimport/{$bodyPage}";
?>
