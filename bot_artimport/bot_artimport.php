<?php
// no direct access
defined('_VALID_MOS') or die('Direct Access to this location is not allowed.');
ini_set("soap.wsdl_cache_enabled", "0"); // ��������� ����������� WSDL
$_MAMBOTS->registerFunction('onAfterStart', 'importArticles');

class GetArticlesRequest {
    public $userName;
    public $password;
    public $channels = array();
}

class AddArticleRequest {
    public $userName;
    public $password;
    public $articles = array();
}

class Article {
    public $channel;
    public $fullText;
    public $id;
    public $introText;
    public $metaDescription;
    public $metaKeywords;
    public $publishDate;
    public $title;
}

function importArticles() {
    global $database;

    $option = mosGetParam($_REQUEST, "option", '');

    if (trim(strtolower($option)) != 'com_artexport') {
        $database->setQuery("UPDATE #__artimport_config SET last_updated = NOW() WHERE last_updated < DATE_SUB(NOW(), INTERVAL update_interval HOUR) AND active = 1");
        $database->query();

        if ($database->getAffectedRows() > 0) {

            //������ ������
            $database->setQuery("SELECT * FROM #__artimport_config WHERE active = 1");
            $settings = $database->loadObjectList();

            $setting = $settings[0];

            $request = new GetArticlesRequest();
            $request->userName = $setting->username;
            $request->password = $setting->password;

            $database->setQuery("SELECT id FROM  #__artimport_import_channels WHERE active = 1");
            $channels = $database->loadObjectList();

            if (sizeof($channels > 0)) {
                foreach ($channels as $dbChannel) {
                    $request->channels[] = $dbChannel->id;
                }

                $client = new SoapClient("http://chugaga.net/components/com_artexport/ArtExport.wsdl");

                $articlesResp = $client->getArticles($request);

                if (isset($articlesResp->articles)) {
                    $sections = array();

                    $articles = array();

                    if (is_array($articlesResp->articles)) {
                        $articles = $articlesResp->articles;
                    } else {
                        $articles[] = $articlesResp->articles;

                    }
                    foreach ($articles as $article) {
                        $channel = $article->channel;
                        $category = null;
                        $section = null;
                        $autopublish = null;
                        $mainpage = null;

                        if (isset($sections[$channel])) {
                            $category = $sections[$channel][0];
                            $section = $sections[$channel][1];
                            $autopublish = $sections[$channel][2];
                            $mainpage = $sections[$channel][3];
                        } else {
                            $sql = "SELECT category_id, `section`, autopublish, mainpage FROM  #__artimport_import_channels as channels\n"
                                    . "LEFT OUTER JOIN #__categories as categories ON categories.id = channels.category_id\n"
                                    . "WHERE channels.id = " . $channel;

                            $database->setQuery($sql);
                            $dbSections = $database->loadObjectList();

                            $category = $dbSections[0]->category_id;
                            $section = $dbSections[0]->section;
                            $autopublish = $dbSections[0]->autopublish;
                            $mainpage = $dbSections[0]->mainpage;

                            $sections[$channel][0] = $category;
                            $sections[$channel][1] = $section;
                            $sections[$channel][2] = $autopublish;
                            $sections[$channel][3] = $mainpage;
                        }

                        $sqlUpdateArticleOrdering = "update #__content set ordering = ordering + 1 where sectionid = " . $section . " and catid = " . $category;
                        $database->setQuery($sqlUpdateArticleOrdering);
                        $database->query();

                        $sqlInsertArticle = "insert into #__content (version, title, title_alias, introtext, `fulltext`, state, sectionid, mask, catid, created, created_by, created_by_alias, modified, modified_by, checked_out, checked_out_time, publish_up, images, urls, attribs, parentid, ordering, metakey, metadesc, access, hits) values
                                                        (0, " . $database->Quote(iconv("UTF-8", "CP1251", $article->title)) . ", " . $database->Quote(iconv("UTF-8", "CP1251", $article->title)) . ", " . $database->Quote(iconv("UTF-8", "CP1251", $article->introText)) . ", " . $database->Quote(iconv("UTF-8", "CP1251", $article->fullText)) . ", " . $autopublish . ", " . $section . ", 0, " . $category . ", " . $database->Quote($article->publishDate) . ", " . (int) $setting->article_user_owner_id . ", '', " . $database->Quote($article->publishDate) . ", " . (int) $setting->article_user_owner_id . ", 0, '0000-00-00 00:00:00', " . $database->Quote($article->publishDate) . ", '', '', 'pageclass_sfx=
back_button=
item_title=1
link_titles=
introtext=1
section=0
section_link=0
category=0
category_link=0
rating=
author=
createdate=
modifydate=
pdf=
print=
email=
keyref=
docbook_type=', 0, 1, " . $database->Quote(iconv("UTF-8", "CP1251", $article->metaKeywords)) . ", " . $database->Quote(iconv("UTF-8", "CP1251", $article->metaDescription)) . ", 0, 0)";

                        $database->setQuery($sqlInsertArticle);
                        $database->query();
                        $articleId = $database->insertid();

                        $database->setQuery("INSERT INTO #__artimport_imported_articles (article_id, ext_article_id) values (" . $articleId . ", " . (int) $article->id . ")");
                        $database->query();

                        if ($mainpage == 1) {
                            $sqlUpdateFrontpage = "update #__content_frontpage set ordering = ordering + 1";
                            $database->setQuery($sqlUpdateFrontpage);
                            $database->query();

                            $sqlInsertIntoFrontpage = "insert into #__content_frontpage(ordering, content_id, imported) values (1, " . $articleId . ", NOW())";
                            $database->setQuery($sqlInsertIntoFrontpage);
                            $database->query();
                        }
                    }
                }
            }

            //������� ������
            $sql = "SELECT #__artimport_export_channels.id as channel_id, #__content.id as article_id, title, introtext, `fulltext`, publish_up, metakey, metadesc FROM #__content, #__artimport_export_channels\n"
                    . "WHERE state = 1 AND catid = category_id AND active = 1 AND #__content.id NOT IN (\n"
                    . "SELECT article_id FROM #__artimport_exported_articles)";

            $database->setQuery($sql);
            $articlesToExport = $database->loadObjectList();

            if (isset($articlesToExport) && sizeof($articlesToExport) > 0) {
                $exportedArticles = array();
                if (is_array($articlesToExport)) {
                    $exportedArticles = $articlesToExport;
                } else {
                    $exportedArticles[] = $articlesToExport;
                }

                $exportRequest = new AddArticleRequest();
                $exportRequest->userName = $setting->username;
                $exportRequest->password = $setting->password;

                $siteConfig =& JFactory::getConfig();
                foreach ($exportedArticles as $articleToExport) {
                    $exportingArticle = new Article();
                    $fullText = replaceUrls($articleToExport->fulltext)
                            . "<p><a href=\"" . JURI::current() . "\" target=\"_blank\">" . $siteConfig->getValue('config.sitename') . "</a></p>";

                    $exportingArticle->channel = $articleToExport->channel_id;
                    $exportingArticle->fullText = replaceUrls(iconv("CP1251", "UTF-8", $fullText));
                    $exportingArticle->id = $articleToExport->article_id;
                    $exportingArticle->introText = replaceUrls(iconv("CP1251", "UTF-8", $articleToExport->introtext));
                    $exportingArticle->metaDescription = iconv("CP1251", "UTF-8", $articleToExport->metadesc);
                    $exportingArticle->metaKeywords = iconv("CP1251", "UTF-8", $articleToExport->metakey);
                    $exportingArticle->publishDate = $articleToExport->publish_up;
                    $exportingArticle->title = iconv("CP1251", "UTF-8", $articleToExport->title);

                    $exportRequest->articles[] = $exportingArticle;
                }

                $addArticlesResp = $client->addArticle($exportRequest);

                if (isset($addArticlesResp->ids) && sizeof($addArticlesResp->ids) > 0) {
                    $ids = array();
                    if (is_array($addArticlesResp->ids)) {
                        $ids = $addArticlesResp->ids;
                    } else {
                        $ids[] = $addArticlesResp->ids;
                    }

                    foreach ($ids as $extId) {
                        $insertIdSql = "INSERT INTO #__artimport_exported_articles (article_id, ext_article_id, imported) VALUES (" . (int) $extId->key . ", " . (int) $extId->value . ", NOW())";
                        $database->setQuery($insertIdSql);
                        $database->query();
                    }
                }
            }
        }
    }
}

function replaceUrls($text) {
    $url = JURI::current();

    $newText = str_replace("src=\"images", "src=\"" . $url . "/images", $text);
    $newText = str_replace("src=\"/images", "src=\"" . $url . "/images", $newText);
    $newText = str_replace("src='images", "src='" . $url . "/images", $newText);
    $newText = str_replace("src='/images", "src='" . $url . "/images", $newText);
    $newText = str_replace("href='/", "src='" . $url . "/", $newText);
    $newText = str_replace("href=\"/", "src=\"" . $url . "/", $newText);

    return $newText;
}

?>