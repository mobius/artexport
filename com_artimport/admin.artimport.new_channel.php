<?php
defined('_VALID_MOS') or die('Direct Access to this location is not allowed.');

class ChannelRequest {
    public $userName;
    public $password;
}

$channelId = intval(mosGetParam($_REQUEST, "channel_id", null));
$channelName = "";
$category = null;
$section = null;
$autoPublish = 1;
$mainPage = 1;
$active = 1;

if ($channelId != null) {
    $database->setQuery("SELECT channels.*, #__categories.section FROM #__artimport_channels as channels\n" .
            "LEFT OUTER JOIN #__categories ON #__categories.id = channels.category_id WHERE channels.id = " . $channelId);
    $dbChannels = $database->loadObjectList();

    $channelName = $dbChannels[0]->channel_name;
    $category = $dbChannels[0]->category_id;
    $section = $dbChannels[0]->section;
    $active = $dbChannels[0]->active;
    $autoPublish = $dbChannels[0]->autopublish;
    $mainPage = $dbChannels[0]->mainpage;
}

function formMenu() {
    echo "<table width='100%' border='0'>\n"
            . "<tr>\n"
            . "    <td width='100%'>&nbsp;</td>\n"
            . "    <td align='center'>\n"
            . "        <a class='toolbar' onclick='saveChannel()' href='#'>\n"
            . "            <img src='/administrator/images/save_f2.png' alt='���������' name='save' title='���������'\n"
            . "                 align='middle' border='0'>\n"
            . "            <br>���������\n"
            . "        </a>\n"
            . "    </td>\n"
            . "    <td width='10'>&nbsp;</td>\n"
            . "    <td align='center'>\n"
            . "        <a class='toolbar' onclick='applyChannel()' href='#'>\n"
            . "            <img src='/administrator/images/apply_f2.png' alt='���������' name='apply' title='���������'\n"
            . "                 align='middle' border='0'>\n"
            . "            <br>���������\n"
            . "        </a>\n"
            . "    </td>\n"
            . "    <td width='10'>&nbsp;</td>\n"
            . "    <td align='center'>\n"
            . "        <a class='toolbar' href='?option=com_artimport&operation=channels_list'>\n"
            . "            <img src='/administrator/images/cancel_f2.png' alt='������' name='cancel' title='������'\n"
            . "                 align='middle' border='0'>\n"
            . "            <br>������\n"
            . "        </a>\n"
            . "    </td>\n"
            . "</tr>\n"
            . "</table>\n";
}

$database->setQuery("SELECT * FROM #__artimport_config");
$settings = $database->loadObjectList();
$setting = $settings[0];

if ($setting->username == null || $setting->username == "" || !isset($setting->username)) {
    echo "<table width='100%' cellspacing='0' cellpadding='0' border='0'>"
            . "<tr><td align='center'><b>������ ��� ��������� �����, ���������� <a href='?option=com_artimport&operation=settings'>��������� �������</a>!</b></td></tr></table>";
}

$request = new ChannelRequest();

$request->userName = $setting->username;
$request->password = $setting->password;

echo "<script type='text/javascript'>\n"
        . "    var categories = new Array();\n"
        . "    var channels = new Array();\n";

$database->setQuery("SELECT #__categories.* FROM #__categories, #__sections where #__categories.section = #__sections.id and #__categories.published = 1 ORDER BY #__categories.section");
$categories = $database->loadObjectList();

foreach ($categories as $dbCategory) {
    echo "if (categories[" . $dbCategory->section . "] == undefined) {categories[" . $dbCategory->section . "] = new Array();}\n"
            . "categories[" . $dbCategory->section . "][categories[" . $dbCategory->section . "].length] = [" . $dbCategory->id . ", " . $database->Quote($dbCategory->title) . "];\n";
}

$client = new SoapClient("http://chugaga.net/components/com_artexport/ArtExport.wsdl");

$channelRequest = new ChannelRequest();

$channelRequest->userName = $setting->username;
$channelRequest->password = $setting->password;

$getChannelsResponse = $client->getChannels($channelRequest);
$channels = array();
if (is_array($getChannelsResponse->channels)) {
    $channels = $getChannelsResponse->channels;
} else {
    $channels[] = $getChannelsResponse->channels;
}

foreach ($channels as $channel) {
    $extChannelId = $channel->id;
    if ($extChannelId != null && $extChannelId != "") {
        echo "channels[" . $extChannelId . "] = " . $database->Quote(iconv("UTF-8", "CP1251", $channel->name)) . ";\n";
    }
}

echo "</script>";

if (sizeof($getChannelsResponse->messages) > 0) {
    $messages = $getChannelsResponse->messages;
    echo "<table width='100%' cellspacing='0' cellpadding='0' border='0'>";
    if (is_array($messages)) {
        foreach ($messages as $message) {
            echo "<tr><td align='center'><b>" . $message . "</b></td></tr>";
        }
    } else {
        echo "<tr><td align='center'><b>" . $messages . "</b></td></tr>";
    }
    echo "</table>";

}
formMenu();
?>

<script type="text/javascript">

    function validateData() {
        var categorySelect = document.getElementById("category");
        var sectionSelect = document.getElementById("section");
        var channelSelect = document.getElementById("channel_id");

        if (channelSelect.selectedIndex == "") {
            alert("���������� ������� ����������� �����");
            return false;
        }

        if (sectionSelect.selectedIndex == "") {
            alert("���������� ������� ������, ���� ����� ����������� ������");
            return false;
        }

        if (categorySelect.selectedIndex == "") {
            alert("���������� ������� ���������, ���� ����� ����������� ������");
            return false;
        }

        return true;
    }

    function saveChannel() {
        if (validateData()) {
            var operation = document.getElementById("operation");
            var theForm = document.getElementById("theForm");
            operation.value = "save_channel";
            theForm.submit();
            return true;
        }
        return false;
    }

    function applyChannel() {
        if (validateData()) {
            var operation = document.getElementById("operation");
            var theForm = document.getElementById("theForm");
            operation.value = "apply_channel";
            theForm.submit();
            return true;
        }
        return false;
    }

    function onSectionChange() {
        var categorySelect = document.getElementById("category");
        var sectionSelect = document.getElementById("section");
        var channelSelect = document.getElementById("channel_id");
        var channelName = document.getElementById("channel_name");

        if (channels[channelSelect.options[channelSelect.selectedIndex].value] != undefined) {
            channelName.value = channels[channelSelect.options[channelSelect.selectedIndex].value];

            while (categorySelect.options.length > 1) {
                categorySelect.remove(1);
            }

            var sectionValue = sectionSelect.options[sectionSelect.selectedIndex].value;
            if (sectionValue != '' && sectionValue != null && sectionValue != undefined &&
                    categories[sectionValue] != undefined) {
                for (var i = 0; i < categories[sectionValue].length; i++) {
                    var objOption = document.createElement("option");
                    objOption.text = categories[sectionValue][i][1];
                    objOption.value = categories[sectionValue][i][0];

                    if (document.all && !window.opera) {
                        categorySelect.add(objOption);
                    } else {
                        categorySelect.add(objOption, null);
                    }
                }
            }
        }
    }
</script>

<form method="POST" id="theForm">
    <input type="hidden" id="operation" name="operation" value="save_channel">
    <input type="hidden" id="channel_name" name="channel_name" value="<?= $channelName ?>">

    <table class="adminheading" width="100%" border="0">
        <tr>
            <th class="edit" colspan="4">�������������� ������</th>
        </tr>
        <tr>
            <td nowrap="nowrap" colspan="4">
                <input type="radio" name="active" <?=($active == 1) ? "checked=\"checked\"" : ""?> value="1"
                       id="active_checkbox">
                <label for="active_checkbox">��������</label><br/>
                <input type="radio" name="active" value="0"
                       id="nonactive_checkbox" <?=($active == 0) ? "checked=\"checked\"" : ""?>>
                <label for="nonactive_checkbox">�� �������� <a></a></label>
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap"><label for="channel_id">������������� �����:</label></td>
            <td width="20">&nbsp;</td>
            <td>
                <select id="channel_id" name="channel_id">
                    <option value=""> -�������� �����-</option>
                <?php
                foreach ($channels as $channel) {
                    echo "<option value='" . $channel->id . "'" . (($channelId == $channel->id) ? " selected" : "") . ">" . iconv("UTF-8", "CP1251", $channel->name) . "</option>";
                }
                ?>
                </select>
            </td>
            <td width="100%">&nbsp;</td>
        </tr>
        <tr>
            <td nowrap="nowrap"><label for="section">������:</label></td>
            <td width="20">&nbsp;</td>
            <td>
                <select id="section" name="section" onchange="onSectionChange()">
                    <option value=""> -�������� �����-</option>
                <?php
                $database->setQuery("SELECT * FROM #__sections where published = 1 ORDER BY ordering");
                $sections = $database->loadObjectList();

                foreach ($sections as $sectionRow) {
                    echo "<option value='" . $sectionRow->id . "'" . (($sectionRow->id == $section) ? " selected" : "") . ">" . $sectionRow->title . "</option>\n";
                }
                ?>
                </select>
            </td>
            <td width="100%">&nbsp;</td>
        </tr>
        <tr>
            <td nowrap="nowrap"><label for="category">���������:</label></td>
            <td width="20">&nbsp;</td>
            <td>
                <select id="category" name="category">
                    <option value=""> -�������� �����-</option>
                <?php
                    if ($category != null) {
                    $database->setQuery("SELECT * FROM #__categories where published = 1 and section=" . $section . " ORDER BY ordering");
                    $categories = $database->loadObjectList();

                    foreach ($categories as $categoryRow) {
                        echo "<option value='" . $categoryRow->id . "'" . (($categoryRow->id == $category) ? " selected" : "") . ">" . $categoryRow->title . "</option>\n";
                    }
                }
                ?>
                </select>
            </td>
            <td width="100%">&nbsp;</td>
        </tr>
        <tr>
            <td nowrap="nowrap">����������� �������������:</td>
            <td width="20">&nbsp;</td>
            <td>
                <input id="autopublishYes" name="autopublish" type="radio"
                       value="1" <?=($autoPublish == 1) ? "checked=\"checked\"" : ""?>><label
                    for="autopublishYes">��</label>&nbsp;
                <input id="autopublishNo" name="autopublish" type="radio"
                       value="0" <?=($autoPublish == 0) ? "checked=\"checked\"" : ""?>><label
                    for="autopublishNo">���</label>
            </td>
            <td width="100%">&nbsp;</td>
        </tr>
        <tr>
            <td nowrap="nowrap">���������� �� �������:</td>
            <td width="20">&nbsp;</td>
            <td>
                <input id="mainpageYes" name="mainpage" type="radio"
                       value="1" <?=($mainPage == 1) ? "checked=\"checked\"" : ""?>><label
                    for="mainpageYes">��</label>&nbsp;
                <input id="mainpageNo" name="mainpage" type="radio"
                       value="0" <?=($mainPage == 0) ? "checked=\"checked\"" : ""?>><label for="mainpageNo">���</label>
            </td>
            <td width="100%">&nbsp;</td>
        </tr>
    </table>
</form>

<?php
if ($category == null) {
    echo "<script type='text/javascript'>\n"
            . "    onSectionChange();\n"
            . "</script>\n";
}
?>


