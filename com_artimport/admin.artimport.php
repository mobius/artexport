<?php
Error_Reporting(E_ALL & ~E_NOTICE);
defined('_VALID_MOS') or die('Direct Access to this location is not allowed.');

function deleteChannel($channelId) {
    global $database;
    if (is_array($channelId)) {
        if (sizeof($channelId) > 0) {
            $sql = "DELETE FROM #__artimport_channels WHERE id IN (";
            $commaFlag = false;
            foreach ($channelId as $id) {
                if ($commaFlag) {
                    $sql = $sql . ", ";
                }
                $sql = $sql . $id;
            }
            $sql = $sql . ")";
            $database->setQuery($sql);
            $database->query();
        }
    } else {
        $database->setQuery("DELETE FROM #__artimport_channels WHERE id = " . (int) $channelId);
        $database->query();
    }
}

function saveChannel($channelId, $category, $channelName, $autoPublish, $mainPage, $active) {
    global $database;

    deleteChannel($channelId);

    $database->setQuery("INSERT INTO #__artimport_channels (id, category_id, channel_name, autopublish, mainpage, active) values ("
            . (int) $channelId . ", " . (int) $category . ", " . $database->Quote($channelName) . ", " . $autoPublish . ", "
            . $mainPage . ", " . (int) $active . ")");
    $database->query();
}

function activateChannel($channelId) {
    global $database;

    $database->setQuery("UPDATE #__artimport_channels set active = 1 WHERE id = " . (int) $channelId);
    $database->query();
}

function inactivateChannel($channelId) {
    global $database;

    $database->setQuery("UPDATE #__artimport_channels set active = 0 WHERE id = " . (int) $channelId);
    $database->query();
}

function saveSettings($username, $password, $articleOwner, $updateInterval) {
    global $database;

    if ($password != "") {
        $database->setQuery("UPDATE #__artimport_config set username = " . $database->Quote($username) . ", password = "
                . $database->Quote($password) . ", article_user_owner_id = " . $articleOwner . ", update_interval = " . $updateInterval);
    } else {
        $database->setQuery("UPDATE #__artimport_config set username = " . $database->Quote($username) .
                ", article_user_owner_id = " . $articleOwner . ", update_interval = " . $updateInterval);
    }
    $database->query();
}

//��������, ������� ����� �������������
$operation = mosGetParam($_REQUEST, "operation", null);
//������������� ������
$channelId = intval(mosGetParam($_REQUEST, "channel_id", null));
//�������������� �������
$channelIds = mosGetParam($_REQUEST, "channel_ids", null);
//��� ������
$channelName = mosGetParam($_REQUEST, "channel_name", null);
//������������� ���������
$category = intval(mosGetParam($_REQUEST, "category", null));
//���� ����������
$active = intval(mosGetParam($_REQUEST, "active", 1));
$userName = mosGetParam($_REQUEST, "userName", null);
$password = mosGetParam($_REQUEST, "password", null);
$autoPublish = intval(mosGetParam($_REQUEST, "autopublish", 1));
$mainPage = intval(mosGetParam($_REQUEST, "mainpage", 1));
$articleOwner = mosGetParam($_REQUEST, "articleowner", 62);
$updateInterval = mosGetParam($_REQUEST, "updateinterval", 1);

$bodyPage = "admin.artimport.list_import_channels.php";
$messages = "";

switch ($operation) {
    case "import_channel":
        $bodyPage = "admin.artimport.import_channel.php";
        break;

    case "save_channel":
        saveChannel($channelId, $category, $channelName, $autoPublish, $mainPage, $active);
        $messages = "����� ������� ��������";
        break;

    case "apply_channel":
        $bodyPage = "admin.artimport.import_channel.php";
        saveChannel($channelId, $category, $channelName, $autoPublish, $mainPage, $active);
        $messages = "����� ������� ��������";
        break;

    case "activate_channel":
        activateChannel($channelId);
        $messages = "����� �����������";
        break;

    case "inactivate_channel":
        inactivateChannel($channelId);
        $messages = "����� �������������";
        break;

    case "delete_channel":
        deleteChannel($channelIds);
        $messages = "��������� ������ �������";
        break;

    case "settings":
        $bodyPage = "admin.artimport.settings.php";
        break;

    case "save_settings":
        saveSettings($userName, $password, $articleOwner, $updateInterval);
        $messages = "��������� ���������";
        break;

    case "apply_settings":
        $bodyPage = "admin.artimport.settings.php";
        saveSettings($userName, $password, $articleOwner, $updateInterval);
        $messages = "��������� ���������";
        break;
}

if ($messages != null && $messages != "") {
    echo "<table width='100%' cellspacing='0' cellpadding='0' border='0'>"
            . "<tr><td align='center'><b>" . $messages . "</b></td></tr></table>";

    $messages = "";
}

include "components/com_artimport/{$bodyPage}";
?>
