<?php
defined('_VALID_MOS') or die('Direct Access to this location is not allowed.');

function formMenu() {
    echo "<table width='100%' border='0'>\n"
            . "<tr>\n"
            . "    <td width='100%'>&nbsp;</td>\n"
            . "    <td align='center'>\n"
            . "        <a class='toolbar' onclick='saveSettings()' href='#'>\n"
            . "            <img src='/administrator/images/save_f2.png' alt='���������' name='save' title='���������'\n"
            . "                 align='middle' border='0'>\n"
            . "            <br>���������\n"
            . "        </a>\n"
            . "    </td>\n"
            . "    <td width='10'>&nbsp;</td>\n"
            . "    <td align='center'>\n"
            . "        <a class='toolbar' onclick='applySettings()' href='#'>\n"
            . "            <img src='/administrator/images/apply_f2.png' alt='���������' name='apply' title='���������'\n"
            . "                 align='middle' border='0'>\n"
            . "            <br>���������\n"
            . "        </a>\n"
            . "    </td>\n"
            . "    <td width='10'>&nbsp;</td>\n"
            . "    <td align='center'>\n"
            . "        <a class='toolbar' href='?option=com_artimport&operation=channels_list'>\n"
            . "            <img src='/administrator/images/cancel_f2.png' alt='������' name='cancel' title='������'\n"
            . "                 align='middle' border='0'>\n"
            . "            <br>������\n"
            . "        </a>\n"
            . "    </td>\n"
            . "</tr>\n"
            . "</table>\n";
}

$database->setQuery("SELECT * FROM #__artimport_config");
$settings = $database->loadObjectList();
$setting = $settings[0];

formMenu();
?>

<script type="text/javascript">

    function saveSettings() {
        var operation = document.getElementById("operation");
        var theForm = document.getElementById("theForm");
        operation.value = "save_settings";
        theForm.submit();
        return true;
    }

    function applySettings() {
        var operation = document.getElementById("operation");
        var theForm = document.getElementById("theForm");
        operation.value = "apply_settings";
        theForm.submit();
        return true;
    }
</script>


<form method="POST" id="theForm">
    <input type="hidden" id="operation" name="operation" value="save_channel">
    <table class="adminheading" width="100%" border="0">
        <tr>
            <th class="edit" colspan="5">��������� �������</th>
        </tr>
        <tr>
            <td nowrap="nowrap"><label for="userName">������������:</label></td>
            <td width="20" rowspan="4">&nbsp;</td>
            <td>
                <input id="userName" name="userName" type="text" value="<?=$setting->username?>">
            </td>
            <td width="20" rowspan="4">&nbsp;</td>
            <td width="100%" rowspan="2">
                <small>
                    ������������ ������ ���� <a
                        href="http://chugaga.com/component/option,com_comprofiler/task,registers/" target="_blank">���������������</a>
                    �� ����� <a href="http://chugaga.com" target="_blank">chugaga.com</a>
                </small>
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap"><label for="password">������:</label></td>
            <td>
                <input id="password" name="password" type="password">
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap"><label for="updateinterval">�������� ����������:</label></td>
            <td>
                <select id="updateinterval" name="updateinterval">
                    <option value="1"></option>
                <?php
                    for ($i = 0; $i<=24; $i++) {
                    echo "<option value='" . $i . "'".(($setting->update_interval == $i)? " selected":""). ">" . $i . "</option>";
                }
                ?>
                </select>
            </td>
            <td width="100%">
                <small>
                    ����������� ���������� �����, ����� ������� ����� ����������� ������. 0 - ����������� ��� (�� �������������, ��� ��� ��� ������� ������� �������� �� ���� �������).
                </small>
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap"><label for="articleowner">�������� ������:</label></td>
            <td>
                <select id="articleowner" name="articleowner">
                <?php
                $database->setQuery("SELECT * FROM #__users where block=0");
                $users = $database->loadObjectList();
                if (sizeof($users) > 0) {
                    foreach ($users as $user) {
                        echo "<option value='" . $user->id . "'".(($setting->article_user_owner_id == $user->id)? " selected" : "").">" . $user->username . " (" . $user->name . ")</option>";
                    }
                }
                ?>
                </select>
            </td>
            <td width="100%">
                <small>
                    ���������� �������, ���� �� ������������� ������ ������� ����� ������������ ��������������� ������.
                </small>
            </td>
        </tr>
    </table>
</form>