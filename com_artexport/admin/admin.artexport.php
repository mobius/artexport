<?php
defined('_JEXEC') or die('Restricted access');

//require_once("../components/com_artexport/includes/functions.php");

//Операция, которая будет производиться
$operation = mosGetParam($_REQUEST, "operation", null);
//Идентификатор канала
$clanId = intval(mosGetParam($_REQUEST, "channelId", null));

$messages = "";
$bodyPage = "admin.artexport.list.php";

$messages = "";

switch ($operation) {
    case "new_channel":
        setActivateClanFlag($clanId, 1);
        break;

    case "admin_clan_deactivate":
        setActivateClanFlag($clanId, 0);
        break;

    case "admin_clan_check":
        setIsNewClanFlag($clanId, 0);
        break;

    case "admin_clan_delete":
        setDeleteClanFlag($clanId, 1);
        break;

    case "admin_clan_undelete":
        setDeleteClanFlag($clanId, 0);
        break;

    case "admin_clan_remove":
        removeClan($clanId);
        break;
}

include "../components/com_artexport/showmessages.php";
include "components/com_artexport/{$bodyPage}";

function createChannel() {
}

?>
