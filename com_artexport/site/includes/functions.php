<?php
defined('_JEXEC') or die('Restricted access');

/**
 * Checks if user registered
 * @param  $userName User name
 * @param  $passwd User password
 * @return bool
 */
function checkUserRegistered($userName, $passwd) {
    $database = & JFactory::getDBO();

    $sql = "select * from #__users where block = 0 and username = " . $database->Quote($userName);

    $database->setQuery($sql);
    $users = $database->loadObjectList();

    $statusFlag = false;
    if (sizeof($users) > 0) {
        $row = $users[0];
        $parts = explode(':', $row->password);
        if (count($parts) > 1) {
            $salt = $parts[1];
            $crypt = md5($passwd . $salt);
            $hashedPwd = $crypt . ':' . $salt;
            if ($hashedPwd == $row->password) {
                $statusFlag = true;
            }
        }
    }

    return $statusFlag;
}

function getUserConfiguration($userName) {
    $database = & JFactory::getDBO();

    $userConfigSql = "SELECT * FROM #__artexport_user_configuration WHERE username = " . $database->Quote($userName);
    $database->setQuery($userConfigSql);
    $userConfiguration = $database->loadObjectList();
    if (sizeof($userConfiguration) == 0) {
        $userConfigurationInsert = "INSERT INTO #__artexport_user_configuration (username, show_commerce, populate_statistic, show_footer_info, autopublish_import)\n" .
                "VALUES (" . $database->Quote($userName) . ", 1, 1, 1, 0)";

        $database->setQuery($userConfigurationInsert);
        $database->query();

        $database->setQuery($userConfigSql);
        $userConfiguration = $database->loadObjectList();
    }

    return $userConfiguration[0];
}

function getArticleForChannel($userName, $channelId) {
    $database = & JFactory::getDBO();

    $database->setQuery("SELECT category_id FROM #__artexport_channels where active = 1 and id = " . (int) $channelId);
    $channel = $database->loadObjectList();

    $result = array();

    if (sizeof($channel) > 0) {
        $database->setQuery("SELECT * FROM #__artexport_user_content_stat where userName = " . $database->Quote($userName) . " and channel_id = " . (int) $channelId);
        $stat = $database->loadObjectList();
        $lastArticleId = 0;
        if (sizeof($stat) > 0) {
            $lastArticleId = $stat[0]->last_article_id;
        } else {
            $sql = "INSERT into #__artexport_user_content_stat (userName, channel_id, last_article_id) values (" . $database->Quote($userName) . ", " . (int) $channelId . ", 0)";
            $database->setQuery($sql);
            $database->query();
        }

        $channelFilter = "where publish_up >= DATE_SUB(NOW(), INTERVAL 3 DAY) and id > " . (int) $lastArticleId . " and state=1 and catid = " . (int) $channel[0]->category_id;

        $sql = "select max(id) as art_id from #__content\n" . $channelFilter;
        $database->setQuery($sql);
        $artMaxIds = $database->loadObjectList();
        $artMaxId = 0;
        if (sizeof($artMaxIds) > 0) {
            $artMaxId = $artMaxIds[0]->art_id;
        }

        if ($artMaxId > $lastArticleId) {
            $sql = "UPDATE #__artexport_user_content_stat SET last_article_id = " . (int) $artMaxId . " where username = " . $database->Quote($userName) . " and channel_id = " . (int) $channelId;
            $database->setQuery($sql);
            $database->query();

            $sql = "select id, title, introtext, `fulltext`, metakey, metadesc, publish_up\n" .
                    "from #__content\n" .
                    $channelFilter . " and id > " . (int) $lastArticleId . " and id <= " . $artMaxId . " ORDER BY publish_up, id;";

            $database->setQuery($sql);
            $contents = $database->loadObjectList();

            foreach ($contents as $dbContent) {
                $content = new Article();
                $content->channel = $channelId;
                $content->fullText = str_replace("src=\"images", "src=\"http://chugaga.net/images", $dbContent->fulltext);
                $content->id = $dbContent->id;
                $content->introText = str_replace("src=\"images", "src=\"http://chugaga.net/images", $dbContent->introtext);
                $content->title = str_replace("src=\"images", "src=\"http://chugaga.net/images", $dbContent->title);
                $content->metaKeywords = $dbContent->metakey;
                $content->metaDescription = $dbContent->metadesc;
                $content->publishDate = $dbContent->publish_up;

                $result[] = $content;
            }
        }
    }

    return $result;
}

?>