<?php
defined('_JEXEC') or die('Restricted access');
ini_set("soap.wsdl_cache_enabled", "0"); // отключаем кэширование WSDL
require_once("components/com_artexport/includes/functions.php");

class Channel {
    public $id;
    public $name;
    public $language;
}

class Article {
    public $id;
    public $channel;
    public $title;
    public $introText;
    public $fullText;
    public $publishDate;
    public $metaDescription;
    public $metaKeywords;
}

class ArticlesResponse {
    public $messages = array();
    public $articles = array();
}

class ChannelsResponse {
    public $messages = array();
    public $channels = array();
}

class AddArticleResponse {
    public $messages = array();
    public $success;
    public $ids = array();
}

class IntPair {
    public $key;
    public $value;
}

function getChannels($request) {
    $database = & JFactory::getDBO();

    $result = new ChannelsResponse();
    if (!checkUserRegistered($request->userName, $request->password)) {
        $result->messages[] = "Could not find registered user with user name " . $request->userName .
                " and specified password";
    } else {
        $database->setQuery("SELECT * FROM  #__artexport_channels where active = 1");
        $channels = $database->loadObjectList();

        foreach ($channels as $dbChannel) {
            $channel = new Channel();
            $channel->id = $dbChannel->id;
            $channel->name = $dbChannel->channel_name;
            $channel->language = $dbChannel->language;

            $result->channels[] = $channel;
        }
    }

    return $result;
}

function getArticles($request) {
    $result = new ArticlesResponse();
    if (!checkUserRegistered($request->userName, $request->password)) {
        $result->messages[] = "Could not find registered user with user name " . $request->userName .
                " and specified password";
    } else {

        $channels = $request->channels;
        if (is_array($channels)) {
            foreach ($channels as $channel) {
                $articles = getArticleForChannel($request->userName, $channel);
                if (sizeof($articles) > 0) {
                    $result->articles = array_merge($result->articles, $articles);
                }
            }
        } else {
            $articles = getArticleForChannel($request->userName, $channels);
            $result->articles = $articles;
        }
    }

    return $result;
}

function addArticle($request) {
    $result = new AddArticleResponse();
    if (!checkUserRegistered($request->userName, $request->password)) {
        $result->messages[] = "Could not find registered user with user name " . $request->userName .
                " and specified password";
    } else {
        $database = & JFactory::getDBO();

        if (!isset($request->articles) || sizeof($request->articles) == 0) {
            $result->messages[] = "There is no articles to import";
        } else {
            $articlesToImport = array();
            if (is_array($request->articles)) {
                $articlesToImport = $request->articles;
            } else {
                $articlesToImport[] = $request->articles;
            }

            $getUserIdSql = "SELECT id FROM #__users WHERE username = " . $database->Quote($request->userName);
            $database->setQuery($getUserIdSql);
            $userIds = $database->loadObjectList();
            $userId = $userIds[0]->id;

            foreach ($articlesToImport as $articleToImport) {
                $sectionSql = "SELECT category_id, `section` FROM  #__artexport_channels as channels\n"
                        . "LEFT OUTER JOIN #__categories as categories ON categories.id = channels.category_id\n"
                        . "WHERE channels.active = 1 AND channels.id = " . (int) $articleToImport->channel;

                $database->setQuery($sectionSql);
                $channels = $database->loadObjectList();

                if (sizeof($channels) == 0) {
                    $result->messages[] = "Could not find specified channel or channel is not active: #" . $articleToImport->channel;
                } else {
                    $sqlInsertArticle = "insert into #__content (version, title, title_alias, introtext, `fulltext`, state, sectionid, mask, catid, created, created_by, created_by_alias, modified, modified_by, checked_out, checked_out_time, publish_up, images, urls, attribs, parentid, ordering, metakey, metadesc, access, hits) values
                                                (0, " . $database->Quote($articleToImport->title) . ", " . $database->Quote($articleToImport->title) . ", " . $database->Quote($articleToImport->introText) . ", " . $database->Quote($articleToImport->fullText) . ", 0, " . $channels[0]->section . ", 0, " . $channels[0]->category_id . ", " . $database->Quote($articleToImport->publishDate) . ", " . $userId . ", '', " . $database->Quote($articleToImport->publishDate) . ", " . $userId . ", 0, '0000-00-00 00:00:00', " . $database->Quote($articleToImport->publishDate) . ", '', '', 'pageclass_sfx=
back_button=
item_title=1
link_titles=
introtext=1
section=0
section_link=0
category=0
category_link=0
rating=
author=
createdate=
modifydate=
pdf=
print=
email=
keyref=
docbook_type=', 0, 1, " . $database->Quote($articleToImport->metaKeywords) . ", " . $database->Quote($articleToImport->metaDescription) . ", 0, 0)";

                    $database->setQuery($sqlInsertArticle);
                    $database->query();

                    $pair = new IntPair();
                    $pair->key = $articleToImport->id;
                    $pair->value = $database->insertid();

                    $result->ids[] = $pair;
                }
            }
        }
    }

    $result->success = true;
    return $result;
}

$server = new SoapServer("components/com_artexport/ArtExport.wsdl");
$server->addFunction("getChannels");
$server->addFunction("getArticles");
$server->addFunction("addArticle");
$server->handle();
?>
